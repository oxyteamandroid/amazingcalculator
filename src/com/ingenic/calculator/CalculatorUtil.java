package com.ingenic.calculator;

import com.ingenic.iwds.HardwareList;

public class CalculatorUtil {
    /**
     * 判断当前的屏是园还是方形
     * @return
     */
    public static boolean IsCircularScreen() {
        return HardwareList.IsCircularScreen();
    }
}
